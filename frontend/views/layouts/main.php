<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $uid = Yii::$app->user->id;
            $type = common\models\User::find()->where(['id' => $uid])->one();
            if ($type['type'] == 1) {
                $menuItems = [
                    ['label' => 'Permissions', 'items' => [
                            ['label' => 'Permission List', 'url' => ['/rbac/index']],
                            '<li class="divider"></li>',
                            ['label' => 'Clear Permissions', 'url' => ['/rbac/clear']],
                            '<li class="divider"></li>',
                            ['label' => 'Create All Permissions ', 'url' => ['/rbac/create_permission']],
                        ],
                    ],
                    ['label' => 'Create Roles', 'url' => ['/role/index']],
                    ['label' => 'Post', 'url' => ['/post/index']],
                    ['label' => 'Users', 'url' => ['/user/index']],
                        //  ['label' => 'Create user', 'url' => ['/site/signup']],
                ];
            } else {
                $menuItems = [
                    ['label' => 'Add New Permissions', 'url' => ['/rbac/index']],
                    ['label' => 'Create Roles', 'url' => ['/role/index']],
                    ['label' => 'Post', 'url' => ['/post/index']],
                        //  ['label' => 'User Assignment', 'url' => ['/assignment/index']],
                ];
            }
            if (Yii::$app->user->isGuest) {
                $menuItems = [//['label' => 'Signup', 'url' => ['/site/signup']],
                    ['label' => 'Login', 'url' => ['/site/login']]
                ];
            } else {
                $menuItems[] = '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
