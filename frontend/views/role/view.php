<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model common\modules\auth\models\AuthItemChild */

$this->title = $model->parent;
$this->params['breadcrumbs'][] = ['label' => 'Auth Item Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$query = \common\modules\auth\models\AuthItemChild::find()->select('child')->where(['parent'=>$model->parent]);

//$dataProvider= new ArrayDataProvider([
//    'query' => $query,
//    'pagination' => false,
//]);
 $dataProvider = new ActiveDataProvider([
            'query' => \common\modules\auth\models\AuthItemChild::find()->where(['parent'=>$model->parent]),
        ]);?>
<div class="auth-item-child-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'parent' => $model->parent], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'parent' => $model->parent], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'parent',
            ['label'=>'Permissions',
                'attribute'=>'child',
                'value' => function($model){
                return $model->child;}],
          //  'child',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
