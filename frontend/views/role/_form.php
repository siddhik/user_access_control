<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\auth\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */

$items = common\modules\auth\models\AuthItem::find()->all();


$controllerlist = [];
if ($handle = opendir('frontend/controllers')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != ".." && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
            $controllerlist[] = $file;
        }
    }
    closedir($handle);
}
asort($controllerlist);
$fulllist = [];
foreach ($controllerlist as $controller):
    $handle = fopen('frontend/controllers/' . $controller, "r");
    if ($handle) {
        // $i= 0;
        while (($line = fgets($handle)) !== false) {
            $i = 0;
            if (preg_match('/public function action(.*?)\(/', $line, $display)):
                if (strlen($display[1]) > 2):

                    $fulllist[substr($controller, 0, -4)][] = strtolower($display[1]);
                endif;
            endif;
            $i++;
        }
    }
    fclose($handle);
endforeach;
//echo json_encode($fulllist);exit;
//        $permissions = [];
//        for ($i = 0; $i < count($controllerlist); $i++) {
//            $cntrl = substr($controllerlist[$i], 0, -4);
//            for ($j = 0; $j < count($fulllist[$cntrl]); $j++) {
//                $permission = substr($cntrl, 0, -10) . '/' . $fulllist[$cntrl][$j];
//                array_push($permissions, $permission);
//            }
//        }
//echo json_encode($items);exit;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document.body).on('change', '#role', function (e) {
        var role = $(this).val();

        var postTo = 'checkrole';
        var data = {
            role: role
        };
        jQuery.post(postTo, data,
                function (data) {
//                    alert(data);
//                    return false;
                    if (1 == data) {
                        $("#main_error").show();
                        $("#main_error").html('This role already exists.');
                        return false;
                    }
                });
    });
    $(document.body).on('change', '.selectall', function (e) {
        $("#role_error").html("");
        if ($(this).is(':checked')) {
            var value = $(this).val();
            $('.' + value).prop('checked', true);
            $('.' + value).prop('disabled', false);
        } else {
            var value = $(this).val();
            $('.' + value).prop('checked', false);
            $('.' + value).prop('disabled', true);
        }
    });
    function myfunction() {
        var checkCount = document.querySelectorAll('input[id="main_cbox"]:checked').length
        if (checkCount == 0) {
            $("#role_error").show();
            $("#role_error").html("Please assign permissions");
            return false;
        }

    }

</script>
<div class="auth-item-child-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'parent')->textInput(['maxlength' => true, 'id' => 'role']) ?>
    <span id="main_error" style="display:none;color:#a94442"></span>
    <table>
        <?php
        foreach ($controllerlist as $controller) {
            $cntrl = substr($controller, 0, -4);
            $permissions = [];
            for ($j = 0; $j < count($fulllist[$cntrl]); $j++) {
                $permission = substr($cntrl, 0, -10) . '/' . $fulllist[$cntrl][$j];
                array_push($permissions, $permission);
            } //echo json_encode($permissions);exit
            ?>

            <tr>
                <td>
                    <label><input type="checkbox"  name="sample" id="main_cbox" value="cbox_<?php echo substr($controller, 0, -14); ?>" class="selectall"/>  <?php echo substr($controller, 0, -14); ?></label>


                    <ul>
                        <?php foreach ($permissions as $per) { ?>
                            <label class="switchery-margin"><input type="checkbox" style="height:15px;width: 15px" class="js-switch  cbox_<?php echo substr($controller, 0, -14); ?>" disabled id="cbox_<?php echo substr($controller, 0, -14); ?>" value="<?php echo $per; ?>" name="check[]"> <?php echo $per ?>

                            </label></br>  <?php
                        }
                    }
                    ?></ul></td></tr>

        <tr><td></td></tr></table><span id="role_error" style="display:none;color:#a94442"></span>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'onclick' => 'return myfunction();']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

