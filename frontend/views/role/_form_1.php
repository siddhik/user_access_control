<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\auth\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */

$items = common\modules\auth\models\AuthItem::find()->all();
//echo json_encode($items);exit;
?>

<div class="auth-item-child-form">

    <?php $form = ActiveForm::begin(); ?>
    <span style="color:#a94442">(Note:Role name must be unique)</span>
    <?= $form->field($model, 'parent')->textInput(['maxlength' => true]) ?>
    <table>
    <?php for($i=0;$i<count($items);$i++){?>
      <tr>
      <td><input type="checkbox" style="height: 25px;" id="cbox<?php echo $i; ?>" value="<?php echo $items[$i]['name']; ?>" name="check[]" ></td><td>
    <?php echo $items[$i]['name']; }?></td></tr></table>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
