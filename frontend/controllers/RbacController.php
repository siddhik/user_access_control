<?php

namespace frontend\controllers;

use Yii;
use common\modules\auth\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacController extends Controller {

    /**
     * @inheritdoc
     */
   public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {
                            $action = Yii::$app->controller->action->id;
                            $controller = Yii::$app->controller->id;
                            $route = "$controller/$action";
                            $uid = Yii::$app->user->id;
                            $type = \common\models\User::find()->where(['id' => $uid])->one();

                            if (Yii::$app->user->can($route) || $type['type'] == 1) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => AuthItem::find()->where(['type'=>2]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();
//
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//
//            return $this->redirect(['view', 'id' => $model->name]);
//        } else {
//            return $this->render('create', [
//                        'model' => $model,
//            ]);
//        }
        if (!empty($_POST['AuthItem'])) {
            $auth = Yii::$app->authManager;

            $rule = $_POST['AuthItem']['name'];
            // add "createPost" permission
            $permission = $auth->createPermission($rule);
            // $index->description = 'List posts';
            $auth->add($permission);
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreate_permission() {
        $auth = Yii::$app->authManager;

        $controllerlist = [];
        if ($handle = opendir('frontend/controllers')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
                    $controllerlist[] = $file;
                }
            }
            closedir($handle);
        }
        asort($controllerlist);
        $fulllist = [];
        foreach ($controllerlist as $controller):
            $handle = fopen('frontend/controllers/' . $controller, "r");
            if ($handle) {
                // $i= 0;
                while (($line = fgets($handle)) !== false) {
                    $i = 0;
                    if (preg_match('/public function action(.*?)\(/', $line, $display)):
                        if (strlen($display[1]) > 2):

                            $fulllist[substr($controller, 0, -4)][] = strtolower($display[1]);
                        endif;
                    endif;
                    $i++;
                }
            }
            fclose($handle);
        endforeach;

        $permissions = [];
        for ($i = 0; $i < count($controllerlist); $i++) {
            $cntrl = substr($controllerlist[$i], 0, -4);
            for ($j = 0; $j < count($fulllist[$cntrl]); $j++) {
                $permission = substr($cntrl, 0, -10) . '/' . $fulllist[$cntrl][$j];
                array_push($permissions, $permission);
            }
        }
       //   echo json_encode($permissions);exit;
        // add "createPost" permission
        for ($i = 0; $i < count($permissions); $i++) {
            $index = $auth->createPermission($permissions[$i]);
            //   $index->description = 'List posts';
            $auth->add($index);
        }
//        // add "updatePost" permission
//        $update = $auth->createPermission('post/update');
//        $update->description = 'Update post';
//        $auth->add($update);
//
//        $delete = $auth->createPermission('post/delete');
//        $delete->description = 'Delete post';
//        $auth->add($delete);
//
//        $view = $auth->createPermission('post/view');
//        $view->description = 'View post';
//        $auth->add($view);
//
//        $create = $auth->createPermission('post/create');
//        $create->description = 'Create post';
//        $auth->add($create);
        return $this->redirect('index');
    }

    public function actionCreate_role() {
        //   $auth = Yii::$app->authManager;
        //$index = $auth->createPermission('post/index');
        //   $update = $auth->createPermission('post/update');
        //   $delete = $auth->createPermission('post/delete');
        //  $view = $auth->createPermission('post/view');
        //   $create = $auth->createPermission('post/create');
//        $author = $auth->createRole('author');
//        $auth->add($author);
//        $auth->addChild($author, $create);
//        $auth->addChild($author, $index);
//        $auth->addChild($author, $view);
//        $user = $auth->createRole('user');
//        $auth->add($user);
//        $auth->addChild($user, $index);
//        $auth->addChild($user, $view);
//        $admin = $auth->createRole('admin');
//        $auth->add($admin);
//        $auth->addChild($admin, $delete);
//        $auth->addChild($admin, $update);
//        $auth->addChild($admin, $author);

        $auth = Yii::$app->authManager;

        $controllerlist = [];
        if ($handle = opendir('frontend/controllers')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
                    $controllerlist[] = $file;
                }
            }
            closedir($handle);
        }
        asort($controllerlist);
        $fulllist = [];

        foreach ($controllerlist as $controller):
            $handle = fopen('frontend/controllers/' . $controller, "r");
            if ($handle) {
                // $i= 0;
                while (($line = fgets($handle)) !== false) {
                    $i = 0;
                    if (preg_match('/public function action(.*?)\(/', $line, $display)):
                        if (strlen($display[1]) > 2):

                            $fulllist[substr($controller, 0, -4)][] = strtolower($display[1]);
                        endif;
                    endif;
                    $i++;
                }
            }
            fclose($handle);
        endforeach;

        $permissions = [];
        for ($i = 0; $i < count($controllerlist); $i++) {
            $cntrl = substr($controllerlist[$i], 0, -4);
            for ($j = 0; $j < count($fulllist[$cntrl]); $j++) {
                $permission = substr($cntrl, 0, -10) . '/' . $fulllist[$cntrl][$j];
                array_push($permissions, $permission);
            }
        }
        //  echo json_encode($permissions);
        // add "createPost" permission
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        for ($i = 0; $i < count($permissions); $i++) {
            $index = $auth->createPermission($permissions[$i]);
            $auth->addChild($admin, $index);
            //   $index->description = 'List posts';
            //  $auth->add($index);
        }
        return $this->redirect('/user-access/role/index');
    }

    public function actionAssignment() {
        $auth = Yii::$app->authManager;

        //$author = $auth->createRole('author');
        $admin = $auth->createRole('admin');

       // $auth->assign($author, 9);
        $auth->assign($admin, 10);
                return $this->redirect('/user-access/assignment/index');

    }
    
    public function actionClear(){
        
        Yii::$app->db->createCommand("delete from auth_item")->execute();
        
        return $this->redirect('index');
        
    }

}
