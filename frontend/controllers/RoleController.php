<?php

namespace frontend\controllers;

use Yii;
use common\modules\auth\models\AuthItemChild;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoleController implements the CRUD actions for AuthItemChild model.
 */
class RoleController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {
                            $action = Yii::$app->controller->action->id;
                            $controller = Yii::$app->controller->id;
                            $route = "$controller/$action";
                            $uid = Yii::$app->user->id;
                            $type = \common\models\User::find()->where(['id' => $uid])->one();

                            if (Yii::$app->user->can($route) || $type['type'] == 1) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItemChild models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => \common\modules\auth\models\AuthItem::find()->where(['type' => 1]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItemChild model.
     * @param string $parent
     * @param string $child
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItemChild model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItemChild();
        //  echo json_encode($_POST);exit;
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'parent' => $model->parent, 'child' => $model->child]);
//        }
        if (!empty($_POST['AuthItemChild'])) {
            $parent = $_POST['AuthItemChild']['parent'];
            $auth = Yii::$app->authManager;
            $author = $auth->createRole($parent);
            $auth->add($author);

            for ($i = 0; $i < count($_POST['check']); $i++) {
                $index = $auth->createPermission($_POST['check'][$i]);
                $auth->addChild($author, $index);
            }
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItemChild model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     */
    public function actionUpdate($parent) {

        $model = AuthItemChild::find()->where(['parent' => $parent])->one();
        //  echo json_encode($model);exit;

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->db->createCommand("delete  from auth_item where name='$parent'")->execute();
            $parent = $_POST['AuthItemChild']['parent'];
            $auth = Yii::$app->authManager;
            $author = $auth->createRole($parent);
            $auth->add($author);

            for ($i = 0; $i < count($_POST['check']); $i++) {
                $index = $auth->createPermission($_POST['check'][$i]);
                $auth->addChild($author, $index);
            }

            return $this->redirect('index');
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItemChild model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     */
    public function actionDelete($parent) {
        Yii::$app->db->createCommand("delete from auth_item_child where parent='$parent'")->execute();
        Yii::$app->db->createCommand("delete from auth_item where name='$parent'")->execute();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItemChild model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $parent
     * @param string $child
     * @return AuthItemChild the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItemChild::findOne(['parent' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCheckrole(){
        $role = $_POST['role'];
        $roles = Yii::$app->db->createCommand("select * from auth_item where name='$role'")->queryAll();
        if(empty($roles)){
        return 0;
        }else{
            return 1;
        }
    }
}
